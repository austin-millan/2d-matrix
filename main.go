package main

import (
    "fmt"
)

// Tuple for use in slices
type Tuple struct {
    Row    int
    Column int
}

var adjR = []int{0,1,0,-1}
var adjC = []int{-1,0,1,0}

func isValid(matrix [][]int, row, col int) bool {
    rowLen, colLen := len(matrix), len(matrix[0])
    if row < 0 || col < 0 || row >= rowLen || col >= colLen {
        return false
    }
    return true
}

func BFS(matrix [][]int) {
    if len(matrix) == 0 {
        return
    }
    queue := make([]Tuple, 0)
    visited := make([][]bool, len(matrix))
    for i, v := range matrix {
        visited[i] = make([]bool, len(v))
    }
    queue = append(queue, Tuple{0, 0})
    visited[0][0] = true
    bfsRecursive(matrix, queue, visited)
}

func bfsRecursive(matrix [][]int, queue []Tuple, visited [][]bool) {
    if len(queue) == 0 {
        return
    }
    front := queue[0] // pop
    queue = queue[1:] // dequeue (NOTE: memory leak)
    fmt.Println(matrix[front.Row][front.Column])
    for i := 0; i < 4; i++ {
        tup := Tuple{Row: front.Row + adjR[i], Column: front.Column + adjC[i]}
        if !isValid(matrix, tup.Row, tup.Column) || visited[tup.Row][tup.Column]  {
            continue
        }
        visited[tup.Row][tup.Column] = true
        queue = append(queue, tup)
    }
    bfsRecursive(matrix, queue, visited) // LEFT
}

func DFS(matrix [][]int) {
    if len(matrix) == 0 {
        return
    }
    visited := make([][]bool, len(matrix))
    for i, v := range matrix {
        visited[i] = make([]bool, len(v))
    }
    dfsRecursive(matrix, 0, 0, visited)
    return
}

func dfsRecursive(matrix [][]int, row int, col int, visited [][]bool) {
    if !isValid(matrix, row, col) || visited[row][col] {
        return
    }
    visited[row][col] = true
    fmt.Println(matrix[row][col])
    dfsRecursive(matrix, row, col-1, visited) // LEFT
    dfsRecursive(matrix, row+1, col, visited) // DOWN
    dfsRecursive(matrix, row-1, col, visited) // UP
    dfsRecursive(matrix, row, col+1, visited) // RIGHT
    return
}

func main() {
    grid := [][]int{
        {-4,9,4},
        {-5,42,5},
        {-6,99,6},
    }
    fmt.Println("GRID:")
    for _, a := range grid {
        fmt.Println(a)
    }
    fmt.Println("BFS")
    BFS(grid)
    fmt.Println("DFS")
    DFS(grid)
}
